#!/usr/bin/env python

# Copyright <2018> <MOHAMED FAZIL>

# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import rospy
import os
import speech_recognition as sr

from speech_to_text_srv.srv import *
from std_msgs.msg import String
from gtts import gTTS

# Function to request to server
def sentiment_analysis_client(x):
    rospy.wait_for_service('sentiment_analysis')
    try:
        sentiment_analysis = rospy.ServiceProxy('sentiment_analysis', SentimentAnalysis)
        resp1 = sentiment_analysis(x)
        return resp1.sentiment
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


if __name__ == "__main__":
# Takes input from audio recordings and gives output  
        r=sr.Recognizer()
	r.energy_threshold =48000
	with sr.WavFile("/home/fazil/catkin_ws/src/speech_to_text_srv/scripts/Recording_3") as source:
        	audio =r.record(source)
        	print "done"
	try:
        	audio=r.recognize_google(audio)
		print ("I think you said %s"%audio)
	except Exception as e:
        	print (e)
        print "Requesting to server"
        text1=sentiment_analysis_client(audio)
        print text1
        t=gTTS(text=text1,lang='en')
        t.save('myfile.mp3')
        os.system("mpg321 myfile.mp3")
