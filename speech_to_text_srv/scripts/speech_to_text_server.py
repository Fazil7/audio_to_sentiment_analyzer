#!/usr/bin/env python

# Copyright <2018> <MOHAMED FAZIL>

# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from speech_to_text_srv.srv import *
import rospy
# Function to perform sentiment analysis
# Takes request(text) from client as input
def handle_sentiment_analysis(req):
	text=req.a
	a=text.split()
	count=0
        c=0
        c1=0
	positive=['good','smile','happy']
        negative=['angry','unhappy','sad']
        neutral=['ok','nice','hello']
# Checks the presence of input text in lists
	for k in range (0,len(a)):
		if a[k] in neutral:
			c1=c1+1
        	if a[k] in negative:
                	c=c+1
        	if a[k] in positive:
			count=count+1
# According to the count performs sentiment analysis
	if (count > c) and (count > c1):
        	s="sentence is positive"
        	return SentimentAnalysisResponse(s)	# Return response s to client
	elif (c1 > count) and (c1 > c):
        	s="sentence is neutral"
                return SentimentAnalysisResponse(s)
	elif (c >= count) and (c > c1):
        	s="sentence is negative"
        	return SentimentAnalysisResponse(s)
	else:
        	s="invalid"
        	return SentimentAnalysisResponse(s)
def sentiment_analysis_server():
    rospy.init_node('speech_to_text_server')
    s = rospy.Service('sentiment_analysis', SentimentAnalysis, handle_sentiment_analysis)
    rospy.spin()
if __name__ == "__main__":
    sentiment_analysis_server()
