#!/usr/bin/env python
import rospy
from std_msgs.msg import String



def callback(data):
	#printing the string
        rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
        rospy.loginfo(data.data)
	#creating 3 files positive, negative, nuetral
        positive=['good','smile','happy']
        negative=['angry','unhappy','sad']
        neutral=['ok','nice','hello']
 
        for i in positive:
        	if i==data.data:
			s= "the word is positive"
			pub=rospy.Publisher('audio',String,queue_size=10)
			rospy.init_node('listener', anonymous=True)
			rate=rospy.Rate(1)

                while not rospy.is_shutdown():
			y=s
                	rospy.loginfo(y)
                	pub.publish(y)
                	rate.sleep()

                        #os.system("espeak 'it is a positive word'")
                
        for i in negative:
        	if i==data.data:
         		s= "the word is negative"
			pub=rospy.Publisher('audio',String,queue_size=10)
                	rospy.init_node('listener', anonymous=True)
                	rate=rospy.Rate(1)

		while not rospy.is_shutdown():
                	y=s
                	rospy.loginfo(y)
                	pub.publish(y)
                	rate.sleep()
  
                        # os.system("espeak 'it is a negative word'")
	
	for i in neutral:
        	if i==data.data:
                	s="the word is neutral"
			pub=rospy.Publisher('audio',String,queue_size=10)
			rospy.init_node('listener', anonymous=True)
			rate=rospy.Rate(1)

                while not rospy.is_shutdown():
                	y=s
                	rospy.loginfo(y)
                	pub.publish(y)
                	rate.sleep()              
                        #os.system("espeak 'it is a neutral word'")

def listener():

        # In ROS, nodes are uniquely named. If two nodes with the same
        # node are launched, the previous one is kicked off. The
        # anonymous=True flag means that rospy will choose a unique
        # name for our 'listener' node so that multiple listeners can
        # run simultaneously.
	rospy.init_node('listener', anonymous=True)

        rospy.Subscriber("audio", String, callback)

        # spin() simply keeps python from exiting until this node is stopped
        rospy.spin()
       


#p=['good','smile','happy']
#n=['angry','unhappy','sad']
#neu=['ok','nice']
 
#for i in p:
#          if i==data.data:
#                  print("the word is positive")
#                  os.system("espeak 'it is a positive word'")
#for i in n:
#           if i==data.data:
#                   print("the word is negative")
#                   os.system("espeak 'it is a negative word'")
#for i in neu:
#            if i==data.data:
#                   print("the word is neutral")
#                   os.system("espeak 'it is a neutral word'")

if  __name__ == '__main__':
         listener()
#        t2s()

#def t2s():
#	pub=rospy.Publisher('chatter',String,queue_size=10)
#       rospy.init_node('listener', anonymous=True)
#       rate=rospy.Rate(10)
#
#       while not rospy.is_shutdown():
#       	y=s
#               rospy.loginfo(y)
#               pub.publish(y)
#               rate.sleep()


