#!/usr/bin/env python
# Simple talker demo that published std_msgs/Strings messages to the 'chatter' topic

import rospy
from std_msgs.msg import String
import speech_recognition as sr

def talker():
	#to initialising the node
	pub = rospy.Publisher('audio', String, queue_size=10)
	rospy.init_node('talker', anonymous=True)
	rate = rospy.Rate(10) # 10hz
	r = sr.Recognizer()

	with sr.WavFile("Recording_3") as source:
		audio = r.record(source)

	while not rospy.is_shutdown():
		text = r.recognize_google(audio)
		print (text)        
		#to publish the text
        	rospy.loginfo(text)
		pub.publish(text)
		rate.sleep()

if __name__ == '__main__':
	try:
		talker()
	except rospy.ROSInterruptException:
		pass
