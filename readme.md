speech_sentimental_analysis_in_ROS_environment

This project is a python 2.7 based program in ROS environment which is to perform sentiment analysis of 
speech input from user  and gives a voice output saying whether the sentence is positive, negative or neutral. 
The project is done in all three modes of ROS communication:


Publisher/Subscriber
Service/Client
Action Server/Client


Prerequisites:

1.ROS

Install ROS in Linux OS. I am currently using ROS kinetic in ubuntu 16.04 LTS.
Link : http://wiki.ros.org/kinetic/Installation/Ubuntu

2.Pip

Run the following command to update the package list and upgrade all of your system software to the latest version available

sudo apt-get update && sudo apt-get -y upgrade

then install pip 

sudo apt-get install python-pip

3.PyAudio :

sudo apt-get install portaudio19-dev python-all-dev python3-all-dev && sudo pip install pyaudio

4.SpeechRecognition 3.8.1:

pip install SpeechRecognition

Follow this link for more deatils:https://pypi.org/project/SpeechRecognition/

5.gTTS 2.0.0 : 

gTTS (Google Text-to-Speech), a Python library and CLI tool to interface with Google Translate's text-to-speech API

pip install gTTS

After installing all the above things
follow these steps:

1.Create a catkin workspace
 Follow the link :http://wiki.ros.org/catkin/Tutorials/create_a_workspace

2.Create a catkin Package
 Follow the link :http://wiki.ros.org/ROS/Tutorials/catkin/CreatingPackage

3.Writing a  Publisher and Subscriber
 Follow the link :http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29

4.Writing  Service and Client
 Follow the link :http://wiki.ros.org/ROS/Tutorials/WritingServiceClient%28python%29

5.Writing  Action Service and Client
 Follow the link :http://wiki.ros.org/actionlib_tutorials/Tutorials/Writing%20a%20Simple%20Action%20Client%20%28Python%29
               :http://wiki.ros.org/actionlib_tutorials/Tutorials/Writing%20a%20Simple%20Action%20Server%20using%20the%20Execute%20Callback%20%28Python%29
